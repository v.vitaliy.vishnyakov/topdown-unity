using _Core;
using UnityEngine;

public class MutantUI : MonoBehaviour
{
    [SerializeField] private HealthBar _healthBar;
    [SerializeField] private Canvas _canvas;
    public HealthBar HealthBar => _healthBar;
    
    private void Awake()
    {
        SetWorldCamera(GameManager.MainCamera);
    }
    void LateUpdate()
    {
        transform.LookAt(GameManager.MainCamera.transform);
    }
    private void SetWorldCamera(Camera worldCamera)
    {
        _canvas.worldCamera = worldCamera;
    }
}
