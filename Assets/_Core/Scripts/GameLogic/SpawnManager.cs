﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class SpawnManager : Singleton<SpawnManager>
    {
        private delegate bool Condition<in T>(T obj);
        
        [SerializeField] private Mutant _mutantPrefab;
        private List<Mutant> _mutantsPool = new List<Mutant>();

        protected override void Initialize() { }

        public static Mutant SpawnMutant(Vector3 position)
        {
            Mutant mutant =  _instance.Spawn(position, _instance._mutantsPool, _instance._mutantPrefab);
            mutant.Initialize();
            mutant.SetTarget(GameManager.PlayerTransform);
            return mutant;
        }
        
        private T Spawn<T, U>(Vector3 position, List<T> pool, T prefab, Condition<T> condition = null) 
            where T: MonoBehaviour, IPoolableObject
            where U: T
        {
            T obj = null;
            foreach (var poolObj in pool)
            {
                if (poolObj is U && !poolObj.gameObject.activeInHierarchy)
                {
                    if (condition != null)
                    {
                        if (!condition(poolObj))
                        {
                            continue;
                        }
                    }
                    obj = poolObj;
                    break;
                }
            }
            if (obj == null)
            {
                obj = Instantiate(prefab);
                pool.Add(obj);
            }
            obj.gameObject.transform.position = position;
            obj.Enable();
            return obj;       
        }
        private T Spawn<T>(Vector3 position, List<T> pool, T prefab, Condition<T> condition = null) where T : MonoBehaviour, IPoolableObject
        {
            return Spawn<T, T>(position, pool, prefab, condition);
        }
        
        public static void DestroyObject(IPoolableObject gameObject)
        {
            gameObject.Disable();
        }
    }
}