﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class GameManager : Singleton<GameManager>
    {
        [Header("Parameters")]
        [SerializeField] private float _spawnCheckCooldown;
        [SerializeField] private int _minMutantsAtMoment;
        [SerializeField] private int _maxMutantsAtMoment;
        [SerializeField] private LayerMask _environmentLayers;
        [Header("Objects references")]
        [SerializeField] private List<SpawnSpot> _mutantSpawnSpots;
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private GameObject _gameOverScreen;
        [SerializeField] private HealthBar _healthBar;
        
        private int _currentMutantsCount;
        private float _timer;
        public static Camera MainCamera => _instance._mainCamera;
        public static Transform PlayerTransform => _instance._playerController.transform;

        protected override void Initialize() { }

        private void Start()
        {
            _playerController.SubscribeOnDeath(OnPlayerDeath);
            _playerController.SubscribeOnDamageReceived(OnPlayerDamageReceived);
            _healthBar.Initialize(_playerController.InitialHp);
            _healthBar.SetHp(_playerController.HP);
        }

        private void Update()
        {
            _timer += Time.deltaTime;
            if (_timer >= _spawnCheckCooldown)
            {
                SpawnMutants();
                _timer = 0;
            }
        }

        private void SpawnMutants()
        { 
            _timer += Time.deltaTime;
            if (_timer < _spawnCheckCooldown) return;
            if (_currentMutantsCount >= _minMutantsAtMoment) return;
            int range = _maxMutantsAtMoment - _currentMutantsCount;
            int mutantsToSpawn = Random.Range(1, range + 1);
            List<SpawnSpot> spawnSpots = GetInvisibleSpawnSpots();
            if (spawnSpots.Count == 0) return;
            SpawnSpot spawnSpot = spawnSpots[Random.Range(0, spawnSpots.Count)];
            for (int i = 0; i < mutantsToSpawn; i++)
            {
                Mutant mutant = SpawnManager.SpawnMutant(spawnSpot.transform.position);
                mutant.SubscribeOnDeath(OnMutantDeath);
                _currentMutantsCount++;
            }
            _timer = 0;
        }

        private void OnMutantDeath(Mutant mutant)
        {
            _currentMutantsCount--;
            mutant.UnsubscribeOnDeath(OnMutantDeath);
        }

        private List<SpawnSpot> GetInvisibleSpawnSpots()
        {
            List<SpawnSpot> invisibleSpawnSpots = new List<SpawnSpot>();
            foreach (SpawnSpot mutantSpawnSpot in _mutantSpawnSpots)
            {
                if (!Utils.IsVisibleByCamera(mutantSpawnSpot.gameObject, MainCamera, _environmentLayers))
                {
                    invisibleSpawnSpots.Add(mutantSpawnSpot);
                }
            }
            return invisibleSpawnSpots;
        }

        private void OnPlayerDeath()
        {
            GameOver();
        }

        private void GameOver()
        {
            _gameOverScreen.SetActive(true);
            _healthBar.gameObject.SetActive(false);
            Time.timeScale = 0;
        }

        private void OnPlayerDamageReceived(float hpLeft)
        {
            _healthBar.SetHp(hpLeft);
        }
    }
}