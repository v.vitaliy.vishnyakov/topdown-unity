﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Text _healthAmountLabel;

        private float _maxBarValue;
        
        public void Initialize(float maxBarValue)
        {
            _maxBarValue = maxBarValue;
        }
        
        public void SetHp(float healthAmount)
        {
            _image.fillAmount = healthAmount / _maxBarValue;
            if (_healthAmountLabel != null)
            {
                _healthAmountLabel.text = healthAmount.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}