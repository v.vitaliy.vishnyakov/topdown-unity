﻿namespace _Core
{
    public interface IDamageable
    {
        void ReceiveDamage(float damage);
    }
}