﻿using UnityEngine;

namespace _Core
{
    public class CameraBehaviour : MonoBehaviour
    {
        [SerializeField] private bool _smoothMode;
        [SerializeField] private Transform _playerTransform;
        [SerializeField] private LayerMask _rayLayer;
        
        private Vector3 PlayerPosition => _playerTransform.position;
        
        private float xOffset;
        private float zOffset;
        private Camera _cameraComponent;

        private void Awake()
        {
            xOffset = transform.position.x - PlayerPosition.x;
            zOffset = transform.position.z - PlayerPosition.z;
            _cameraComponent = GetComponent<Camera>();
        }

        private void LateUpdate()
        {
            Move();
        }

        private void Move()
        {
            if (_smoothMode)
            {
                Ray ray = _cameraComponent.ScreenPointToRay(Input.mousePosition);
                Physics.Raycast(ray, out RaycastHit raycastHit, Mathf.Infinity, _rayLayer);
                Vector3 mousePosition = raycastHit.point;
                Vector3 middlePosition = Vector3.Lerp(PlayerPosition, mousePosition, 0.25f);
                float x = middlePosition.x - PlayerPosition.x;
                float z = middlePosition.z - PlayerPosition.z;
                transform.position =
                    new Vector3(PlayerPosition.x + xOffset + x, transform.position.y, PlayerPosition.z + zOffset + z);
            }
            else
            {
                transform.position =
                    new Vector3(PlayerPosition.x + xOffset, transform.position.y, PlayerPosition.z + zOffset);
            }
        }
    }
}