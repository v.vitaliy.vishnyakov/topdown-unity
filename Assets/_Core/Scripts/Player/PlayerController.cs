﻿using System;
using UnityEngine;

namespace _Core
{
    public class PlayerController : MonoBehaviour, IDamageable
    {
        private event Action OnDeath;
        private event Action<float> OnDamageReceived;

        private readonly int IsMovingHash = Animator.StringToHash("IsMoving");

        [Header("Parameters")]
        [SerializeField] private float _initialHp;
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _shootingRate;
        [SerializeField] private float _bulletDamage;
        [SerializeField] private Transform _shootPoint;
        [Header("LayerMasks")]
        [SerializeField] private LayerMask _hitMask;
        [SerializeField] private LayerMask _groundLayer;
        [Header("Objects references")]
        [SerializeField] private ParticleSystem _shootEffect;        
        [Header("Components references")]
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private Animator _animator;
        [SerializeField] private Rigidbody _rigidbody;
        
        private bool _isMoving;
        private bool _isShooting;
        private float _timeFromLastShot;
        private float _timeBetweenShots;

        public float HP { get; private set; }
        public float InitialHp => _initialHp;

        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            HP = _initialHp;
            _timeBetweenShots = 1f / _shootingRate;
            _shootEffect.gameObject.SetActive(false);
        }
        
        private void Update()
        {
            Shoot();
        }

        private void FixedUpdate()
        {
            Move();
            Rotate();
        }

        private void Move()
        {
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            float verticalInput = Input.GetAxisRaw("Vertical");
            
            _isMoving = horizontalInput != 0 || verticalInput != 0;
            _animator.SetBool(IsMovingHash, _isMoving);
            
            Vector3 moveVector = new Vector3(horizontalInput, 0, verticalInput);
            _rigidbody.MovePosition(transform.position + moveVector * _moveSpeed * Time.deltaTime);
        }

        private void Rotate()
        {
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out RaycastHit raycastHit, Mathf.Infinity, _groundLayer);
            Vector3 mousePosition = raycastHit.point;
            Vector3 lookDirection = mousePosition - transform.position;
            lookDirection.y = 0;
            _rigidbody.MoveRotation(Quaternion.LookRotation(lookDirection));
        }

        private void Shoot()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _isShooting = true;
                _shootEffect.Play();
                _shootEffect.gameObject.SetActive(true);
            }
            if (Input.GetMouseButton(0))
            {
                _timeFromLastShot += Time.deltaTime;
                if (_timeFromLastShot >= _timeBetweenShots)
                {
                    ShootWithRaycast();
                    _timeFromLastShot = 0;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                _isShooting = false;    
                _shootEffect.Stop();
                _shootEffect.gameObject.SetActive(false);
            }
        }

        private void ShootWithRaycast()
        {
            if (Physics.Raycast(_shootPoint.position, transform.forward, out RaycastHit hit, _hitMask))
            {
                if (hit.transform.TryGetComponent(out IDamageable hitObject))
                {
                    hitObject.ReceiveDamage(_bulletDamage);    
                }
            }
        }

        public void ReceiveDamage(float damage)
        {
            HP -= damage;
            OnDamageReceived?.Invoke(HP);
            if (HP <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            OnDeath?.Invoke();
        }

        public void SubscribeOnDeath(Action onDeathHandler)
        {
            OnDeath += onDeathHandler;
        }
        public void SubscribeOnDamageReceived(Action<float> onDamageReceivedHandler)
        {
            OnDamageReceived += onDamageReceivedHandler;
        }
    }
}