﻿using UnityEngine;

namespace _Core
{
    public static class Utils
    {
        public static bool IsVisibleByCamera(GameObject gameObject, Camera camera, LayerMask visibilityMask)
        {
            Vector3 viewPortPoint = camera.WorldToViewportPoint(gameObject.transform.position);
            if (viewPortPoint.x < 0 || viewPortPoint.x > 1 || viewPortPoint.y < 0 || viewPortPoint.y > 1)
            {
                return false;
            }
            Ray lastRay = camera.ViewportPointToRay(viewPortPoint);
            float distanceToCamera = Vector3.Distance(camera.transform.position, gameObject.transform.position);
            return !Physics.Raycast(lastRay, distanceToCamera, visibilityMask);
        }
    }
}